package com.codegreenllc.sample.dto;

import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import lombok.Data;

@Data
public class TodoDto {
	boolean complete = false;
	UUID id;
	Set<String> tags = new TreeSet<>();
	String title;
	Long version;
}
