package com.codegreenllc.sample.dto;

import java.util.List;

import lombok.Data;
import lombok.Getter;

@Data
public class PageDto<T> {
	@Getter
	private List<T> content;
	@Getter
	private int size;
	@Getter
	private long totalElements;
	@Getter
	private int totalPages;

	public void add(final T dto) {
		content.add(dto);
	}

	public boolean hasContent() {
		return !content.isEmpty();
	}
}
