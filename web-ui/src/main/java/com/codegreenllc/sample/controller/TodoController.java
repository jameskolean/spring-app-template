package com.codegreenllc.sample.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.RedirectView;

import com.codegreenllc.sample.dto.PageDto;
import com.codegreenllc.sample.dto.TodoDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class TodoController {
	String baseUrl = "http://localhost:9001/api";
	PageDto<TodoDto> todos = new PageDto<>();

	private String buildUrl(final String path) {
		return baseUrl + path;
	}

	@SuppressWarnings({ "rawtypes" })
	@GetMapping("/todo")
	public String greeting(final Model model) {
		final RestTemplate restTemplate = new RestTemplate();
		final ResponseEntity<? extends PageDto> response = restTemplate
				.getForEntity(buildUrl("/todos?page=0&pageSize=10"), todos.getClass());
		model.addAttribute("todoForm", new TodoDto());
		model.addAttribute("todoList", response.getBody().getContent());
		return "todo";
	}

	@GetMapping("/")
	public String home(final Model model) {
		return greeting(model);
	}

	@PostMapping("/add-todo")
	public RedirectView todoCreate(@ModelAttribute("todoForm") final TodoDto todoDto,
			@RequestParam(required = false, defaultValue = "0") final int page,
			@RequestParam(required = false, defaultValue = "10") final int pageSize,
			@RequestParam(required = false) final String fields, final Model model) {
		final RestTemplate restTemplate = new RestTemplate();
		log.info("adding {}", todoDto);
		final HttpEntity<TodoDto> request = new HttpEntity<>(todoDto);

		final TodoDto newTodoDto = restTemplate.postForObject(buildUrl("/todos"), request, TodoDto.class);
		log.info("Added {}", newTodoDto);

		return new RedirectView("todo");
	}

	@PostMapping("/delete-todo")
	public RedirectView todoDelete(@ModelAttribute("todoForm") final TodoDto todoDto,
			@RequestParam(required = false, defaultValue = "0") final int page,
			@RequestParam(required = false, defaultValue = "10") final int pageSize,
			@RequestParam(required = false) final String fields, final Model model) {
		final RestTemplate restTemplate = new RestTemplate();
		log.info("deleting {}", todoDto);
		final HttpEntity<TodoDto> request = new HttpEntity<>(todoDto);

		restTemplate.exchange(buildUrl("/todos"), HttpMethod.DELETE, request, TodoDto.class);

		return new RedirectView("todo");
	}
}
