# spring-app-template
[Spring Tool Suite Configuration](SpringToolSuite.md)

## Build and Running
- Build all
`gradle build`
- Run Services
`gradle :todo-service:bootRun`
    - Browse to `http://localhost:9001/swagger-ui.html`
    - Browse to `http://localhost:9001/h2-console`
    - login with:
        - username: admin
        - password: admin
- Run UI
`gradle :web-ui:bootRun`
    - Browse to `http://localhost:9000/`
    - login with:
        - username: user
        - password: user
- Run Dashboard
`gradle :admin:bootRun`
    - Browse to `http://localhost:9900/`
    - click the check mark
    - login with:
        - username: admin
        - password: admin

### Devtools
Spring Devtools improves the development experience by reloading your application when a change is detected. It does this by watching the compiles directories for changes. If your IDE is not configured to auto compile into gradles build directories you can use Gradle to watch for changes in the source code and automatically compile. Open a new command tab and run: 
`gradle -t build`



## Projects
- [This project monitors the other SpringBoot application using their Actuator endpoint](admin.md)
- This project provides the services
- This project provides the UI

## Feature List
*  Multimodule gradle build UI Module and Service Module
*  Thymeleaf
*  DevTools for hot loading
*  Lombok
*  JPA
    * Many to Many
    * CRUD operation
*  JPA Paging
*  H2 with console
*  Liquibase
*  REST Services
*  Swagger UI
*  Squiggly
*  RestTemplate example calling services from the Web-UI server.
*  Config Server
  
## Feature Wish List
*  Thymeleaf Templating
*  Session
*  Validation JSR-303 
*  Spring-retry
*  Configuration Processor
*  REACT Component
*  OAuth
*  Monitoring 
*  Kafka
*  Spring Social