# Admin Project
Monitoring project for all SpringBoot Apps

## Running
- gradle build
- gradle bootRun
- Browse [http://localhost:9900/#/applications](http://localhost:9900/#/applications)