package com.codegreenllc.sample.controller.repository;

import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codegreenllc.sample.entity.TodoEntity;

@Repository
public interface TodoRepository extends JpaRepository<TodoEntity, UUID> {

	Set<TodoEntity> findAllByTitle(String title);

}
