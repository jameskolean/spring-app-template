package com.codegreenllc.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codegreenllc.sample.controller.service.TodoService;
import com.codegreenllc.sample.dto.PageDto;
import com.codegreenllc.sample.dto.TodoCreateDto;
import com.codegreenllc.sample.dto.TodoDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
public class TodoController {
	@Autowired
	TodoService todoService;

	@PostMapping("/todos")
	public TodoDto todoCreate(@RequestBody final TodoCreateDto todoCreateDto) {
		log.info("got {}", todoCreateDto);
		return todoService.createTodo(todoCreateDto);
	}

	@DeleteMapping("/todos")
	public void todoDelete(@RequestBody final TodoCreateDto todoCreateDto) {
		log.info("got {}", todoCreateDto);
		todoService.deleteTodo(todoCreateDto);
	}

	@GetMapping("/todos")
	public PageDto<TodoDto> todoGetPage(@RequestParam(required = false, defaultValue = "0") final int page,
			@RequestParam(required = false, defaultValue = "10") final int pageSize,
			@RequestParam(required = false) final String fields) {
		return todoService.findTodos(PageRequest.of(page, pageSize, Sort.by(Order.by("title"))));
	}
}
