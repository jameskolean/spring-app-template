package com.codegreenllc.sample.controller.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.codegreenllc.sample.controller.repository.TodoRepository;
import com.codegreenllc.sample.dto.PageDto;
import com.codegreenllc.sample.dto.TodoCreateDto;
import com.codegreenllc.sample.dto.TodoDto;
import com.codegreenllc.sample.entity.TodoEntity;

@Service
public class TodoService {

	@Autowired
	TodoRepository todoRepository;

	public TodoDto createTodo(final TodoCreateDto newTodo) {
		TodoEntity todoEntity = new TodoEntity();
		todoEntity.setTitle(newTodo.getTitle());
		todoEntity = todoRepository.save(todoEntity);
		return TodoDto.builder() //
				.id(todoEntity.getId()).version(todoEntity.getVersion()).title(todoEntity.getTitle())
				.complete(todoEntity.isComplete()).build();
	}

	public void deleteTodo(final TodoCreateDto todoCreateDto) {
		final Set<TodoEntity> findAllByTitle = todoRepository.findAllByTitle(todoCreateDto.getTitle());
		todoRepository.deleteAll(findAllByTitle);
	}

	public PageDto<TodoDto> findTodos(final PageRequest pageRequest) {
		final Page<TodoEntity> entitiesPage = todoRepository.findAll(pageRequest);
		final PageDto<TodoDto> pageDto = new PageDto<>(entitiesPage);
		entitiesPage.getContent().stream().forEach(e -> pageDto.add(TodoDto.of(e)));
		return pageDto;
	}

}
