package com.codegreenllc.sample;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	/**
	 * This section defines the user accounts which can be used for authentication
	 * as well as the roles each user has.
	 */
	@Override
	public void configure(final AuthenticationManagerBuilder auth) throws Exception {

		auth.inMemoryAuthentication() //
				.withUser("user").password("{noop}user").roles("USER") //
				.and().withUser("admin").password("{noop}admin").roles("USER", "ADMIN");
	}

	/**
	 * This section defines the security policy for the app. - BASIC authentication
	 * is supported (enough for this REST-based demo) - CSRF headers are disabled
	 * since we are only testing the REST interface, not a web one.
	 *
	 * NOTE: GET is not shown which defaults to permitted.
	 */
	@Override
	protected void configure(final HttpSecurity httpSecurity) throws Exception {

		httpSecurity.httpBasic() //
				.and().authorizeRequests().antMatchers("/api/**").permitAll() //
				.and().authorizeRequests().antMatchers("/console/**").permitAll() //
				.and().authorizeRequests().anyRequest().authenticated() //
				.and().csrf().disable();
		httpSecurity.headers().frameOptions().disable();
	}
}