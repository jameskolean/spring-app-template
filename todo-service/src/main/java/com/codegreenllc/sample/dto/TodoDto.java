package com.codegreenllc.sample.dto;

import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

import com.codegreenllc.sample.entity.TodoEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TodoDto {
	public static TodoDto of(final TodoEntity todoEntity) {
		final TodoDto result = new TodoDto();
		result.complete = todoEntity.isComplete();
		result.id = todoEntity.getId();
		result.title = todoEntity.getTitle();
		result.version = todoEntity.getVersion();
		result.tags = todoEntity.getTags().stream().map(t -> t.getTitle()).collect(Collectors.toSet());
		return result;

	}

	boolean complete;
	UUID id;
	@Default
	Set<String> tags = new TreeSet<>();
	String title;
	Long version;
}
