package com.codegreenllc.sample.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import lombok.Data;
import lombok.Getter;

@Data
public class PageDto<T> {
	@Getter
	private List<T> content;
	@Getter
	private int size;
	@Getter
	private long totalElements;
	@Getter
	private int totalPages;

	public PageDto(final Page<?> page) {
		this.size = page.getSize();
		this.totalElements = page.getTotalElements();
		this.totalPages = page.getTotalPages();
		this.content = new ArrayList<>();
	}

	public void add(final T dto) {
		content.add(dto);
	}

	public boolean hasContent() {
		return !content.isEmpty();
	}
}
