# Spring Tools Suite
## Extensions
- EclEmma Java Code Coverage
- Eclipse Web Developer Tools
- Eclipse XML Editors and Tools
- m2e-apt

## Configurations
- Lombok

## Formatter
- Eclipse [Built-in]

## Save Actions
- Format all Lines
- Organize imports
- Additional
    - Convert control statement bodies to block
    - Convert 'for' loops to enhanced 'for' loops
    - Add final modifier to private fields
    - Add final modifier to method parameters
    - Add final modifier to local variables
    - Use lambda where possible
    - Remove unused imports
    - Add missing '@Override' annotations
    - Add missing '@Override' annotations to implementations of interface methods
    - Add missing '@Deprecated' annotations
    - Remove unnecessary casts
    - Remove redundant modifiers
    - Remove redundant semicolons
    - Sort all members
    - Remove trailing white spaces on all lines
    - Correct indentation
    - Remove redundant type arguments